

This project is a gym application that creates a workout for users from simple information. For example information about how often users work out and how intense the workouts are.

The project is written in java spring with a MVC setup and built with Maven.

To build the project you can use the "mvn compile" command on the command line.

Tests can also be run with the "mvn test" command. You might need to run "mvn clean" if there are any problems.

[![pipeline status](https://gitlab.com/steini96/SimpleGym/badges/main/pipeline.svg)](https://gitlab.com/steini96/SimpleGym/-/commits/main)

[![coverage report](https://gitlab.com/steini96/SimpleGym/badges/main/coverage.svg)](https://gitlab.com/steini96/SimpleGym/-/commits/main)

Here is a link to a generated maven page run by the pipeline
https://steini96.gitlab.io/SimpleGym/

Here is a badge from SonarCloud:
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=steini96_SimpleGym&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=steini96_SimpleGym)
