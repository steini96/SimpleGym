package is.hi.hbv501g2021supportsession;

import is.hi.hbv501g2021supportsession.Persistence.Entities.*;
import is.hi.hbv501g2021supportsession.Persistence.Repositories.ExerciseRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
//@SpringBootTest
public class ExerciseRepositoryTest {


    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private User firstUser;

    private Workout workout;

    UserFitnessInfo userFitnessInfo;

    private Exercise e1;
    private Exercise e2;
    private Exercise e3;

    @Before
    public void setUp() {
        LoginInfo loginInfo = new LoginInfo();
        userFitnessInfo = new UserFitnessInfo();
        firstUser = new User("Þorsteinn", "thi35@hi.is", loginInfo, userFitnessInfo);

        e1 = new Exercise("Push ups", "", 5);
        e2 = new Exercise("Push ups", "", 10);
        e3 = new Exercise("Push ups", "", 20);

        entityManager.persist(e1);
        entityManager.persist(e2);
        entityManager.persist(e3);
        entityManager.flush();
    }

    @Test
    public void FindAllExercises() {
        assertThat(exerciseRepository.findAll().size()).isEqualTo(3);
        assertThat(exerciseRepository.findAll().get(0)).isEqualTo(e1);
    }

}
